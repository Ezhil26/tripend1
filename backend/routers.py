from user.models import User


class UserRouter:
    route_app_labels={'user'}
    def db_for_read(self, model, **hints):
        if model._meta.app_label in self.route_app_labels:
            return 'postgres'
        return None
    def db_for_write(self, model, **hints):
        if model._meta.app_label in self.route_app_labels:
            return 'postgres'
        return None
    def allow_migrate(Self,db,app_label,model_name=User,**hints):
       if app_label in Self.route_app_labels:
           return db =='postgres'
       return None