# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class TempStoreTable(models.Model):
    fms_location = models.TextField(db_column='FMS_Location')  # Field name made lowercase. This field type is a guess.
    store_code = models.TextField(db_column='Store_Code')  # Field name made lowercase. This field type is a guess.
    user_id = models.TextField(db_column='User_ID', primary_key=True)  # Field name made lowercase. This field type is a guess.
    tempstoreid = models.TextField(db_column='TempStoreID', blank=True, null=True)  # Field name made lowercase. This field type is a guess.

    class Meta:
        managed = False
        db_table = 'Temp-Store_Table'


class TempSelectedBoltable(models.Model):
    tempselectedbolid = models.TextField(db_column='TempSelectedBOLID', primary_key=True)  # Field name made lowercase. This field type is a guess.
    fms_location = models.TextField(db_column='FMS Location', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. This field type is a guess.
    user_id = models.TextField(db_column='User ID')  # Field name made lowercase. Field renamed to remove unsuitable characters. This field type is a guess.
    bol_no = models.TextField(db_column='BOL_No', blank=True, null=True)  # Field name made lowercase. This field type is a guess.

    class Meta:
        managed = False
        db_table = 'Temp-selected_BOLTable'
        unique_together = (('tempselectedbolid', 'user_id'),)


class TempStoreTable(models.Model):
    tempstoreid = models.IntegerField(primary_key=True)
    fms_location = models.CharField(max_length=100)
    user_id = models.IntegerField(db_column='user-id')  # Field renamed to remove unsuitable characters.
    store_code = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'Temp_Store_Table'


class TempSelectedBoltable(models.Model):
    tempselectedbolid = models.IntegerField(primary_key=True)
    fms_location = models.CharField(max_length=-1, blank=True, null=True)
    user_id = models.IntegerField()
    bol_no = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'Temp_selected_bolTable'


class TripStoreDetail(models.Model):
    trip = models.ForeignKey('TripTable1', models.DO_NOTHING, db_column='Trip_ID')  # Field name made lowercase.
    trip_store_details_id = models.IntegerField(primary_key=True)
    store_id = models.IntegerField(blank=True, null=True)
    store_eta = models.TimeField(blank=True, null=True)
    store_etd = models.TimeField(blank=True, null=True)
    delivery_seq = models.IntegerField(blank=True, null=True)
    actual_arr_time = models.TimeField(blank=True, null=True)
    actual_dep_time = models.TimeField(blank=True, null=True)
    pod_1 = models.TextField(blank=True, null=True)
    pod_2 = models.TextField(blank=True, null=True)
    origin_id = models.IntegerField(blank=True, null=True)
    store_name = models.TextField(blank=True, null=True)
    created_date = models.TimeField(blank=True, null=True)
    updated_date = models.TimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Trip-Store_Detail'


class TripTable(models.Model):
    trip_table_id = models.IntegerField(db_column='Trip_Table_ID', primary_key=True)  # Field name made lowercase.
    trip_id = models.IntegerField(db_column='Trip_ID', blank=True, null=True)  # Field name made lowercase.
    origin_location = models.CharField(max_length=-1, blank=True, null=True)
    trip_distance = models.IntegerField(db_column='Trip_distance', blank=True, null=True)  # Field name made lowercase.
    trip_time = models.TimeField(db_column='Trip_Time', blank=True, null=True)  # Field name made lowercase.
    destination_type = models.CharField(db_column='Destination_type', max_length=-1, blank=True, null=True)  # Field name made lowercase.
    destination_code = models.CharField(db_column='Destination_Code', max_length=-1, blank=True, null=True)  # Field name made lowercase.
    planned_dispatch = models.TimeField(blank=True, null=True)
    planned_trip_end_time = models.TimeField(blank=True, null=True)
    trip_status = models.CharField(max_length=-1, blank=True, null=True)
    trip_weight = models.IntegerField(blank=True, null=True)
    trip_volume = models.IntegerField(blank=True, null=True)
    store_drops = models.CharField(max_length=-1, blank=True, null=True)
    trip_created_time = models.TimeField(blank=True, null=True)
    vehicle_number = models.IntegerField(blank=True, null=True)
    child_vendor_id = models.IntegerField(blank=True, null=True)
    contract_type = models.CharField(max_length=-1, blank=True, null=True)
    contract_id = models.IntegerField(blank=True, null=True)
    vehicle_weight_utilization = models.IntegerField(blank=True, null=True)
    vehicle_volume_utilization = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Trip_Table'


class TripTable1(models.Model):
    trip_id = models.IntegerField(db_column='Trip_ID', primary_key=True)  # Field name made lowercase.
    trip_table_id = models.IntegerField(db_column='Trip_Table_ID')  # Field name made lowercase.
    origin_location = models.CharField(max_length=-1, blank=True, null=True)
    trip_distance = models.IntegerField(db_column='Trip_distance', blank=True, null=True)  # Field name made lowercase.
    trip_time = models.TimeField(db_column='Trip_Time', blank=True, null=True)  # Field name made lowercase.
    destination_type = models.CharField(db_column='Destination_type', max_length=-1, blank=True, null=True)  # Field name made lowercase.
    destination_code = models.CharField(db_column='Destination_Code', max_length=-1, blank=True, null=True)  # Field name made lowercase.
    planned_dispatch = models.TimeField(blank=True, null=True)
    planned_trip_end_time = models.TimeField(blank=True, null=True)
    trip_status = models.CharField(max_length=-1, blank=True, null=True)
    trip_weight = models.IntegerField(blank=True, null=True)
    trip_volume = models.IntegerField(blank=True, null=True)
    store_drops = models.IntegerField(blank=True, null=True)
    trip_created_time = models.TimeField(blank=True, null=True)
    vehicle_number = models.IntegerField(blank=True, null=True)
    child_vendor_id = models.IntegerField(blank=True, null=True)
    contract_type = models.CharField(max_length=-1, blank=True, null=True)
    contract_id = models.IntegerField(blank=True, null=True)
    vehicle_weight_utilization = models.IntegerField(blank=True, null=True)
    vehicle_volume_utilization = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Trip_Table1'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class FmsBolHeader(models.Model):
    bol_no = models.TextField(primary_key=True)
    fms_location_master_id = models.IntegerField()
    ship_date = models.DateField()
    bol_value = models.DecimalField(max_digits=6, decimal_places=2)
    bol_weight = models.DecimalField(max_digits=6, decimal_places=2)
    bol_volume = models.DecimalField(max_digits=6, decimal_places=2)
    from_loc = models.IntegerField()
    to_loc = models.IntegerField()
    origin_type = models.TextField()
    trip_id = models.IntegerField(db_column='trip_Id', blank=True, null=True)  # Field name made lowercase.
    shipment = models.IntegerField()
    created_date = models.DateField()
    updated_date = models.DateField()

    class Meta:
        managed = False
        db_table = 'fms_bol_header'


class FmsContract(models.Model):
    contract_id = models.AutoField(primary_key=True)
    fms_origin_vendor_id = models.IntegerField()
    payload_in_mt = models.DecimalField(max_digits=6, decimal_places=2)
    contract_type = models.TextField()
    is_active = models.BooleanField()
    created_date = models.DateField()
    updated_date = models.DateField()

    class Meta:
        managed = False
        db_table = 'fms_contract'


class FmsLocationMaster(models.Model):
    fms_location_master_id = models.AutoField(primary_key=True)
    retek_location_id = models.IntegerField()
    fms_location_id = models.IntegerField()
    fms_location_name = models.TextField()
    fms_origin_flag = models.BooleanField()
    format = models.TextField()
    latitude = models.DecimalField(max_digits=6, decimal_places=4)
    longitude = models.DecimalField(max_digits=6, decimal_places=4)
    pincode = models.IntegerField()
    created_date = models.DateField()
    updated_date = models.DateField()

    class Meta:
        managed = False
        db_table = 'fms_location_master'


class FmsTrip(models.Model):
    trip_id = models.AutoField(primary_key=True)
    trip_dist_fms_est = models.DecimalField(max_digits=6, decimal_places=2)
    trip_time_fms_est = models.DateTimeField()
    dest_type = models.TextField()
    dest_id = models.IntegerField()
    plan_dispatch = models.DateTimeField()
    trip_status = models.TextField()
    trip_wt = models.DecimalField(max_digits=6, decimal_places=2)
    trip_vol = models.DecimalField(max_digits=6, decimal_places=2)
    store_drops = models.IntegerField()
    trip_created_time = models.DateTimeField()
    vehicle_num = models.TextField()
    fms_origin_vendor_id = models.IntegerField()
    vehicle_wt_util = models.DecimalField(max_digits=6, decimal_places=2)
    vehicle_vol_util = models.DecimalField(max_digits=6, decimal_places=2)
    truck_img = models.TextField()
    trip_dist_dc_input = models.DecimalField(max_digits=6, decimal_places=2)
    toll_charges = models.DecimalField(max_digits=6, decimal_places=2)
    halting_charges = models.DecimalField(max_digits=6, decimal_places=2)
    other_charges = models.DecimalField(max_digits=6, decimal_places=2)
    actual_trip_end_time = models.DateTimeField()
    dcm_comments = models.TextField()
    freight_cost_fms = models.DecimalField(max_digits=6, decimal_places=2)
    freight_cost_dc_input = models.DecimalField(max_digits=6, decimal_places=2)
    ntm_trip_cost = models.DecimalField(max_digits=6, decimal_places=2)
    approval_status = models.TextField()
    created_date = models.DateField()
    updated_date = models.DateField()
    contract_id = models.OneToOneField(FmsContract, models.DO_NOTHING)
    fms_location_master_id = models.ForeignKey(FmsLocationMaster, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'fms_trip'
