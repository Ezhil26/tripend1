from django.db.models.fields import CharField
from django.db.models.query import QuerySet
from django.http import response
from django.shortcuts import render
from rest_framework import serializers, status,permissions
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics
from .models import TempStoreTable,TempSelectedBoltable
from .serializers import TempStoreTableSerializer,TempSelectedBolTableSerializer
# Create your views here.


# class TempStoreTableView(generics.ListAPIView):
#     queryset = TempStoreTable.objects.all()
#     serializer_class =TempStoreTableSerializer
#     filterset_fields = ['fms_location', 'user_id']
class TempStoreTableView(APIView):
    permission_classes=(permissions.AllowAny,)
    def post(self,request):
        try:
            fms_location=request.data['fms_location']
            user_id=request.data['user_id']
            fms_location=int(fms_location)
            user_id=int(user_id)
            serializer=TempStoreTableSerializer(TempStoreTable,many=True)
            if serializer.is_valid():
             serializer.save()
             fms_location=serializer.data('fms_location',None)
             user_id=serializer.data('user_id',None)
             return Response(serializer.filter(fms_location=fms_location,user_id=user_id),status=status.HTTP_201_CREATED)
        except:
            return Response({'processed':0,'message':'please provide valid request'},status=status.HTTP_400_BAD_REQUEST)
        # try:
        #     serializer=TempStoreTableSerializer(data=request.data)
        #     if serializer.is_valid():
        #      serializer.save()
        #      fms_location=serializer.data('fms_location',None)
        #      user_id=serializer.data('user_id',None)
        #      return Response(serializer.filter(fms_location=fms_location,user_id=user_id),status=status.HTTP_201_CREATED)
        # except:
        #      return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
       
        # if fms_location is None:
        #     return Response({'processed':0,'message':'fms_location is not present'},status=status.HTTP_404_NOT_FOUND)
        # if user_id is None:
        #     return Response({'processed':0,'message':'user_id is not present'},status=status.HTTP_404_NOT_FOUND)
        # if fms_location=='':
        #     return Response({'processed':0,'message':'fms_location is not present'},status=status.HTTP_404_NOT_FOUND)
        # if user_id=='':
        #     return Response({'processed':0,'message':'user_id is not present'},status=status.HTTP_404_NOT_FOUND)
        # TempStoreTableData=serializer.filter(fms_location=fms_location,user_id=user_id)
        # serializer=TempStoreTableSerializer(TempStoreTableData,many=True)
        # if len(serializer.data)==0:
        #     return Response({'processed':0,'message':'No data found'},status=status.HTTP_404_NOT_FOUND)
        # return Response({'processed':1,'data':serializer.data},status=status.HTTP_200_OK)
        # TempStoreTableData=TempStoreTable.objects.all()
        # serializer=TempStoreTableSerializer(TempStoreTableData,many=True)
        # return Response(serializer.data)
    # def post(self,request):
    #    serializer=TempStoreTableSerializer(data=request.data)
    #    if serializer.is_valid():
    #        serializer.save()
    #        return Response(serializer.data,status=status.HTTP_201_CREATED)
    #    return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
        

class TempSelectedBolTableView(APIView):
    permission_classes=(permissions.AllowAny,)
    def get(self,request):
        serializer=TempSelectedBoltable.objects.all()
        fms_location=self.request.query_params.get('fms_location',None)
        user_id=self.request.query_params.get('user_id',None)
        if fms_location is None:
            return Response({'processed':0,'message':'fms_location is not present'},status=status.HTTP_404_NOT_FOUND)
        if user_id is None:
            return Response({'processed':0,'message':'user_id is not present'},status=status.HTTP_404_NOT_FOUND)
        if fms_location=='':
            return Response({'processed':0,'message':'fms_location is not present'},status=status.HTTP_404_NOT_FOUND)
        if user_id=='':
            return Response({'processed':0,'message':'user_id is not present'},status=status.HTTP_404_NOT_FOUND)
        TempSelectedBoltableData=serializer.filter(fms_location=fms_location,user_id=user_id)
        serializer=TempSelectedBolTableSerializer(TempSelectedBoltableData,many=True)
        if len(serializer.data)==0:
            return Response({'processed':0,'message':'No data found'},status=status.HTTP_404_NOT_FOUND)
        return Response({'processed':1,'data':serializer.data},status=status.HTTP_200_OK)
      