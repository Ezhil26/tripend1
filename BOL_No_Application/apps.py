from django.apps import AppConfig


class BolNoApplicationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'BOL_No_Application'
