from rest_framework import serializers
from .models import TempStoreTable,TempSelectedBoltable

class TempStoreTableSerializer(serializers.ModelSerializer):
    class Meta:
        model=TempStoreTable
        fields="__all__"
class TempSelectedBolTableSerializer(serializers.ModelSerializer):
    class Meta:
        model=TempSelectedBoltable
        fields="__all__"