from django.db import models
from django.db.models import fields
from rest_framework import serializers
from .models import BolHeader, LocationMaster


# Response Serializers
class BolHeaderSerializer(serializers.ModelSerializer):
    class Meta:
        model = BolHeader
        fields = '__all__'


class LocationMasterSerializer(serializers.ModelSerializer):
    class Meta:
        model = LocationMaster
        fields = '__all__'


# Request Serializers
class getPendingBolRequestSerializer(serializers.Serializer):
    fmsLocationId = serializers.IntegerField(required=True)
    stores = serializers.ListField(required=True, min_length=1)
