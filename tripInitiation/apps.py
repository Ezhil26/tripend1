from django.apps import AppConfig


class TripinitiationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tripInitiation'
