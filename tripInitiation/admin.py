from django.contrib import admin
from .models import BolHeader, Trip, LocationMaster, Contract

# Register your models here.
admin.site.register(BolHeader)
admin.site.register(Trip)
admin.site.register(LocationMaster)
admin.site.register(Contract)

