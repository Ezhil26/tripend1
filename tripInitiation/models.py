from django.db import models

# Create your models here.
class LocationMaster(models.Model):
    retek_location_id = models.AutoField(primary_key=True)
    fms_location_id = models.IntegerField()
    fms_location_name = models.TextField()
    fms_origin_flag = models.BooleanField()
    format = models.TextField()
    latitude = models.DecimalField(max_digits=6, decimal_places=4)
    longitude = models.DecimalField(max_digits=6, decimal_places=4)
    pincode = models.IntegerField()
    created_date = models.DateField(auto_now_add=True)
    updated_date = models.DateField(auto_now=True)
    class Meta:
        db_table = "fms_location_master"
        indexes = [
            models.Index(fields=['fms_location_id', 'retek_location_id'])
        ]

class BolHeader(models.Model):
    bol_no = models.TextField(primary_key=True)
    fms_location_master_id = models.IntegerField()
    ship_date = models.DateField()
    bol_value = models.DecimalField(max_digits=6, decimal_places=2)
    bol_weight = models.DecimalField(max_digits=6, decimal_places=2)
    bol_volume = models.DecimalField(max_digits=6, decimal_places=2)
    from_loc = models.IntegerField()
    to_loc = models.IntegerField()
    origin_type = models.TextField()
    trip_Id = models.IntegerField(null=True)
    shipment = models.IntegerField()
    created_date = models.DateField(auto_now_add=True)
    updated_date = models.DateField(auto_now=True)
    location_info=models.ForeignKey(LocationMaster,on_delete=models.CASCADE)
    def __str__(self):
      return self.bol_no

    class Meta:
        db_table = "fms_bol_header"
        indexes = [
            models.Index(fields=['bol_no','fms_location_master_id', 'ship_date', 'trip_Id', 'shipment'])
        ]




class Contract(models.Model):
    contract_id = models.AutoField(primary_key=True)
    fms_origin_vendor_id = models.IntegerField()
    payload_in_mt = models.DecimalField(max_digits=6, decimal_places=2)
    contract_type = models.TextField()
    is_active = models.BooleanField()
    created_date = models.DateField(auto_now_add=True)
    updated_date = models.DateField(auto_now=True)

    class Meta:
        db_table = "fms_contract"
        indexes = [
            models.Index(fields=['contract_id', 'fms_origin_vendor_id'])
        ]


class Trip(models.Model):
    trip_id = models.AutoField(primary_key=True)
    fms_location_master_id = models.ForeignKey(LocationMaster, on_delete=models.PROTECT)
    trip_dist_fms_est = models.DecimalField(max_digits=6, decimal_places=2)
    trip_time_fms_est = models.DateTimeField()
    dest_type = models.TextField()
    dest_id = models.IntegerField()
    plan_dispatch = models.DateTimeField()
    trip_status = models.TextField()
    trip_wt = models.DecimalField(max_digits=6, decimal_places=2)
    trip_vol = models.DecimalField(max_digits=6, decimal_places=2)
    store_drops = models.IntegerField()
    trip_created_time = models.DateTimeField()
    vehicle_num = models.TextField()
    fms_origin_vendor_id = models.IntegerField()
    contract_id = models.ForeignKey(Contract, on_delete=models.PROTECT, unique=True)
    vehicle_wt_util = models.DecimalField(max_digits=6, decimal_places=2)
    vehicle_vol_util = models.DecimalField(max_digits=6, decimal_places=2)
    truck_img = models.TextField()
    trip_dist_dc_input = models.DecimalField(max_digits=6, decimal_places=2)
    toll_charges = models.DecimalField(max_digits=6, decimal_places=2)
    halting_charges = models.DecimalField(max_digits=6, decimal_places=2)
    other_charges = models.DecimalField(max_digits=6, decimal_places=2)
    actual_trip_end_time = models.DateTimeField()
    dcm_comments = models.TextField()
    freight_cost_fms = models.DecimalField(max_digits=6, decimal_places=2)
    freight_cost_dc_input = models.DecimalField(max_digits=6, decimal_places=2)
    ntm_trip_cost = models.DecimalField(max_digits=6, decimal_places=2)
    approval_status = models.TextField()
    created_date = models.DateField(auto_now_add=True)
    updated_date = models.DateField(auto_now=True)

    class Meta:
        db_table = "fms_trip"
        indexes = [
            models.Index(fields=['fms_location_master_id'])
        ]
