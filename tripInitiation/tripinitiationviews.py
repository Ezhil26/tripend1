from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status
from tripInitiation.serializers import BolHeaderSerializer, getPendingBolRequestSerializer, LocationMasterSerializer
from tripInitiation.models import BolHeader, LocationMaster


# Create your views here.

class PendingBOL(APIView):
    """
    Get all pending bols, Get pending bols by store
    """
    permission_classes = [AllowAny]

    def get(self, request):
        bols = BolHeader.objects.all()
        serializer = BolHeaderSerializer(bols, many=True)
        if serializer:
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def post(self, request):
        serializer = getPendingBolRequestSerializer(data=request.data)
        if serializer.is_valid():
          pendingBols = BolHeader.objects.filter(
          to_loc__in=request.data['stores'])
          serializer = BolHeaderSerializer(pendingBols, many=True)
          pendingboldata=BolHeader.objects.filter(location_info=serializer).select_related(BolHeader)
        if pendingboldata:
            return Response(pendingboldata.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)                                            
