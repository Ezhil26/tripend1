from django.urls import path
from .import views


urlpatterns = [
    path('allPendingBols', views.PendingBOL.as_view(), name='All Pending BOLs'),
    path('getPendingBols', views.PendingBOL.as_view(), name='Get Pending BOLs'),
]
