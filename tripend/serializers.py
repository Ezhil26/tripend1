from rest_framework import serializers
from rest_framework.fields import ReadOnlyField
from tripend.models import TripTable,TripStoreDetail


        
class TripTableSerializer(serializers.ModelSerializer):
    class Meta:
        model=TripTable
        fields=["trip_status"]
     
class TripDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model=TripTable
        fields="__all__"
    # def get_trip_status(self,instance):
    #         trip_status_instances=TripTable.objects.filter(trip_status="created")
    #         return TripDetailSerializer(trip_status_instances,many=True).data
# class Tripidserializer(serializers.ModelSerializer):
#     class Meta:
#         model=TripTable
#         fields=["trip_id"]

class TripSerializer(serializers.ModelSerializer):
    class Meta:
        model=TripTable
        fields=['trip_id']

class TripStoreDetailSerializer(serializers.ModelSerializer):
    # trip_id=TripDetailSerializer()
    trip_id=TripDetailSerializer(read_only=True,many=True)
    class Meta:
        model=TripStoreDetail
        fields=('trip_store_details_id','trip_id','store_id','store_name','store_eta','store_etd','delivery_seq','actual_arr_time','actual_dep_time','pod_1','pod_2','origin_id','created_date','updated_date')

class UpdateSerializer(serializers.Serializer):
    trip_id=serializers.IntegerField(required=True)
    store_code=serializers.IntegerField(required=True)
    store_name=serializers.CharField(required=True)
    store_eta=serializers.TimeField(required=True)
    store_etd=serializers.TimeField(required=True)
    store_dsl=serializers.CharField(required=True)
    delivery_sequence=serializers.IntegerField(required=True)
    actual_arrival_time=serializers.TimeField(required=True)
    actual_departure_time=serializers.TimeField(required=True)
    trip_store_detail_id=serializers.IntegerField(required=True)
    def create(self,validated_data):
      return TripStoreDetail.objects.create(**validated_data)