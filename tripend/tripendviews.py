from django.http.response import Http404
from django.shortcuts import render
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import response, serializers
from rest_framework import status
from .models import TripTable,TripStoreDetail
from .serializers import TripTableSerializer,TripStoreDetailSerializer,TripDetailSerializer,UpdateSerializer,TripSerializer
# Create your views here.
class tripEndView(APIView):
  def get(self, request):
        TripsCreated = TripTable.objects.filter(trip_status="created")
        tripserializer=TripSerializer(TripsCreated,many=True)
        tripId=[]
        for values in tripserializer.data:
            tripId.append(values['trip_id'])
        Trips=TripStoreDetail.objects.all().filter(trip_id__in=tripId)
        serializer= TripStoreDetailSerializer(Trips,many=True)
        if serializer:
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  # def put(self, request):
  #       storeDetail = TripStoreDetail.objects.get()
  #       data=request.data
  #       storeDetail.trip_id=data['trip_id']
  #       storeDetail.store_code=data['store_code']
  #       storeDetail.store_name=data['store_name']
  #       storeDetail.store_eta=data['store_eta']
  #       storeDetail.store_etd=data['store_etd']
  #       storeDetail.store_dsl=data['store_dsl']
  #       storeDetail.delivery_sequence=data['delivery_sequence']
  #       storeDetail.actual_arrival_time=data['actual_arrival_time']
  #       storeDetail.actual_departure_time=data['actual_departure_time']
  #       storeDetail.trip_store_detail_id=data['trip_store_detail_id']
  #       storeDetail.save()
  #       serializer = TripStoreDetailSerializer(storeDetail, data=request.data)
  #       return response(serializer.data)
#   def put(self, request):
#        serializer = timeserializer(data=request.data)
#        if serializer.is_valid():
#             updatetime= TripStoreDetail.objects.filter(store_code=['store_code'])
#             serializer = TripStoreDetailSerializer(updatetime, many=True)
#             if serializer:
#                 return Response(serializer.data, status=status.HTTP_200_OK)
#             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  # def get_object(self, store_id):
  #       try:
  #           return TripStoreDetail.objects.get(store_id=store_id)
  #       except TripStoreDetail.DoesNotExist:
  #           raise Http404
  # def put(self,store_id,request):
  #      storeDetail=self.get_object(store_id)
  #     #  print(storeDetail)
  #     #  for stores in storeDetail:
  #     #      print(stores)
  #      serializer=TripStoreDetailSerializer(storeDetail,data=request.data)
  #      print(serializer)
  #      if serializer.is_valid():
  #       serializer.save()
  #      return Response("Success", status=status.HTTP_200_OK)
        # storeDetail=serializer

        # tripserializer= TripStoreDetailSerializer(serializer,many=True)
        # print(tripserializer)
        # if tripserializer:
        #       return Response(tripserializer.data, status=status.HTTP_200_OK)
        # return Response(tripserializer.errors, status=status.HTTP_400_BAD_REQUEST)
class tripEndViewStore(APIView):
  print("*")
  # def put(self, request):
  #       storeDetail = TripStoreDetail.objects.get()
  #       data=request.data
  #       storeDetail.trip_id=data['trip_id']
  #       storeDetail.store_code=data['store_code']
  #       storeDetail.store_name=data['store_name']
  #       storeDetail.store_eta=data['store_eta']
  #       storeDetail.store_etd=data['store_etd']
  #       storeDetail.store_dsl=data['store_dsl']
  #       storeDetail.delivery_sequence=data['delivery_sequence']
  #       storeDetail.actual_arrival_time=data['actual_arrival_time']
  #       storeDetail.actual_departure_time=data['actual_departure_time']
  #       storeDetail.trip_store_detail_id=data['trip_store_detail_id']
  #       storeDetail.save()
  #       serializer = TripStoreDetailSerializer(storeDetail, data=request.data)
  #       return response(serializer.data)
#   def put(self, request):
#        serializer = timeserializer(data=request.data)
#        if serializer.is_valid():
#             updatetime= TripStoreDetail.objects.filter(store_code=['store_code'])
#             serializer = TripStoreDetailSerializer(updatetime, many=True)
#             if serializer:
#                 return Response(serializer.data, status=status.HTTP_200_OK)
#             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  def get_object(self, store_id):
        try:
            return TripStoreDetail.objects.get(store_id=store_id)
        except TripStoreDetail.DoesNotExist:
            raise Http404
  def put(self,request,store_id,format=None):
       print(store_id,request.data)
       storeDetail=TripStoreDetail.objects.all()
       print("----",storeDetail,"----")
       serializer=TripStoreDetailSerializer(storeDetail,data=request.data)
       print(serializer)
       print(request.data)
       print("****************")
       if serializer.is_valid():
         serializer.save()
         return Response("Success", status=status.HTTP_200_OK)
       print(serializer.errors)
       return Response("failure", status=status.HTTP_200_OK)
        # storeDetail=serializer
        # tripserializer= TripStoreDetailSerializer(serializer,many=True)
        # print(tripserializer)
        # if tripserializer:
        #       return Response(tripserializer.data, status=status.HTTP_200_OK)
        # return Response(tripserializer.errors, status=status.HTTP_400_BAD_REQUEST)
# class tripEndViewTrip(APIView):
#   print("*")
#   # def put(self, request):
#   #       storeDetail = TripStoreDetail.objects.get()
#   #       data=request.data
#   #       storeDetail.trip_id=data['trip_id']
#   #       storeDetail.store_code=data['store_code']
#   #       storeDetail.store_name=data['store_name']
#   #       storeDetail.store_eta=data['store_eta']
#   #       storeDetail.store_etd=data['store_etd']
#   #       storeDetail.store_dsl=data['store_dsl']
#   #       storeDetail.delivery_sequence=data['delivery_sequence']
#   #       storeDetail.actual_arrival_time=data['actual_arrival_time']
#   #       storeDetail.actual_departure_time=data['actual_departure_time']
#   #       storeDetail.trip_store_detail_id=data['trip_store_detail_id']
#   #       storeDetail.save()
#   #       serializer = TripStoreDetailSerializer(storeDetail, data=request.data)
#   #       return response(serializer.data)
# #   def put(self, request):
# #        serializer = timeserializer(data=request.data)
# #        if serializer.is_valid():
# #             updatetime= TripStoreDetail.objects.filter(store_code=['store_code'])
# #             serializer = TripStoreDetailSerializer(updatetime, many=True)
# #             if serializer:
# #                 return Response(serializer.data, status=status.HTTP_200_OK)
# #             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#   def get_object(self, trip_id):
#         try:
#             return TripTable.objects.get(trip_id=trip_id)
#         except TripTable.DoesNotExist:
#             raise Http404
#   def put(self,request,trip_id,format=None):
#        print(trip_id,request.data)
#        tripDetail=TripTable.objects.all()
#        print("----",tripDetail,"----")
#        serializer=TripTableSerializer(tripDetail,data=request.data, many=True)
#        print(serializer)
#        print(request.data)
#        if serializer.is_valid():
#          serializer.save()
#          return Response("Success", status=status.HTTP_200_OK)
#        print(serializer.errors)
#        return Response("failure", status=status.HTTP_200_OK)