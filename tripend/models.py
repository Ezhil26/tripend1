# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

class TripTable(models.Model):
    trip_id = models.IntegerField(db_column='Trip_ID', primary_key=True)  # Field name made lowercase.
    trip_table_id = models.IntegerField(db_column='Trip_Table_ID')  # Field name made lowercase.
    origin_location = models.CharField(max_length=50, blank=True, null=True)
    trip_distance = models.IntegerField(db_column='Trip_distance', blank=True, null=True)  # Field name made lowercase.
    trip_time = models.TimeField(db_column='Trip_Time', blank=True, null=True)  # Field name made lowercase.
    destination_type = models.CharField(db_column='Destination_type', max_length=50, blank=True, null=True)  # Field name made lowercase.
    destination_code = models.CharField(db_column='Destination_Code', max_length=50, blank=True, null=True)  # Field name made lowercase.
    planned_dispatch = models.TimeField(blank=True, null=True)
    planned_trip_end_time = models.TimeField(blank=True, null=True)
    trip_status = models.CharField(max_length=50, blank=True, null=True)
    trip_weight = models.IntegerField(blank=True, null=True)
    trip_volume = models.IntegerField(blank=True, null=True)
    store_drops = models.IntegerField(blank=True, null=True)
    trip_created_time = models.TimeField(blank=True, null=True)
    vehicle_number = models.IntegerField(blank=True, null=True)
    child_vendor_id = models.IntegerField(blank=True, null=True)
    contract_type = models.CharField(max_length=50, blank=True, null=True)
    contract_id = models.IntegerField(blank=True, null=True)
    vehicle_weight_utilization = models.IntegerField(blank=True, null=True)
    vehicle_volume_utilization = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Trip_Table1'

class TripStoreDetail(models.Model):
    trip_id = models.ForeignKey(TripTable, models.DO_NOTHING, db_column='Trip_ID')  # Field name made lowercase.
    trip_store_details_id = models.IntegerField(primary_key=True)
    store_id = models.IntegerField(blank=True, null=True)
    store_eta = models.TimeField(blank=True, null=True)
    store_etd = models.TimeField(blank=True, null=True)
    delivery_seq = models.IntegerField(blank=True, null=True)
    actual_arr_time = models.TimeField(blank=True, null=True)
    actual_dep_time = models.TimeField(blank=True, null=True)
    pod_1 = models.TextField(blank=True, null=True)
    pod_2 = models.TextField(blank=True, null=True)
    origin_id = models.IntegerField(blank=True, null=True)
    store_name = models.TextField(blank=True, null=True)
    created_date = models.TimeField(blank=True, null=True)
    updated_date = models.TimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Trip-Store_Detail'


















