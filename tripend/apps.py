from django.apps import AppConfig


class TripendConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tripend'
